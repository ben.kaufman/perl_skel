#!/usr/bin/env perl

use strict;
use warnings;
use FindBin qw($RealBin $Script);
use File::Basename;
use File::Copy;

my ($dirname, $path, $suffix) = fileparse($RealBin);

my $skel = "$RealBin/skel.pl";
my $pl   = "$RealBin/$dirname.pl";
move( $skel, $pl );


my $local_dist = "$RealBin/local_config.yaml.dist";
my $local_conf = "$RealBin/local_config.yaml";

copy( $local_dist, $local_conf );


`git checkout --orphan main`;
`git rm -f $Script local_config.yaml.dist README.md`;
`git add . `;
`git commit -am "Initial Commit"`;
`git branch -D skel`;
`git remote remove origin`;



__END__

=head1 NAME

deploy.pl 

=head1 DESCRIPTION

A simple deployment file for WHOSGONNA (Ben Kaufman) scripts.  This includes
mostly boiler plate for reading in config files and setting up logging. 

=head1 SYNOPSIS

  git clone perl_skel $scriptname
  cd $scriptname
  ./deploy.pl
  cpm install
  carton
  carton exec ./scriptname.pl

=head1 USAGE

For running WHOSGONNA style sripts, first git clone the perl_skel directory to 
a new directory that will have the same name as the desired script. For this
example, the new application will be called 'B<new_app>'

  git clone perl_skel new_app

Next, change to that directory:

  cd new_app

This script (C<deploy.pl>) is then used to:

=over 4

=over 2

=item 1

Checkout a new orphaned git branch named "main".  Delete THIS file (C<deploy.pl>)
and delete the C<local_config.yaml.dist> file.

=item 2

Rename the script C<skel.pl> to match the name of the application. In this 
example, it will be C<new_app.pl>.

=item 3

Add all files to the new git branch.

=item 4

Commit this new branch with a commit message of "Initial Commit".

=item 5

Delete the C<skel> branch that is used in the C<perl_skel> source code.

=item 6

Delete the git remote C<origin>. We no longer want to track perl_skel - this 
should be a new project.

=back

=back

Then run C<cpm install> to install all modules (cpm must be installed).

Run C<carton> once to generate the cpanfile.snapshot.  

Then run C<carton exec ./scriptname.pl> once to run the file.

=cut
