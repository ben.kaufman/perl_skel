# NAME

deploy.pl 

# DESCRIPTION

A simple deployment file for WHOSGONNA (Ben Kaufman) scripts.  This includes
mostly boiler plate for reading in config files and setting up logging. 

# SYNOPSIS

    git clone perl_skel $scriptname
    cd $scriptname
    ./deploy.pl
    cpm install
    carton
    carton exec ./scriptname.pl

# USAGE

For running WHOSGONNA style sripts, first git clone the perl\_skel directory to 
a new directory that will have the same name as the desired script. For this
example, the new application will be called '**new\_app**'

    git clone perl_skel new_app

Next, change to that directory:

    cd new_app

This script (`deploy.pl`) is then used to:

> 1. Checkout a new orphaned git branch named "main".  Delete THIS file (`deploy.pl`)
> and delete the `local_config.yaml.dist` file.
> 2. Rename the script `skel.pl` to match the name of the application. In this 
> example, it will be `new_app.pl`.
> 3. Add all files to the new git branch.
> 4. Commit this new branch with a commit message of "Initial Commit".
> 5. Delete the `skel` branch that is used in the `perl_skel` source code.
> 6. Delete the git remote `origin`. We no longer want to track perl\_skel - this 
> should be a new project.

Then run `cpm install` to install all modules (cpm must be installed).

Run `carton` once to generate the cpanfile.snapshot.  

Then run `carton exec ./scriptname.pl` once to run the file.
